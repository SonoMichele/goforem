package goforem

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"

	"github.com/google/go-querystring/query"
)

type ClientOption func(c *Client)

type Client struct {
	apiKey     string
	baseUrl    string
	httpClient *http.Client

	Articles *Articles
}

type ErrorResponse struct {
	Error  string `json:"error"`
	Status int    `json:"status"`
}

func WithAPIKey(apiKey string) ClientOption {
	return func(c *Client) {
		c.apiKey = apiKey
	}
}

func WithHTTPClient(httpClient *http.Client) ClientOption {
	return func(c *Client) {
		c.httpClient = httpClient
	}
}

func NewClient(baseUrl string, opts ...ClientOption) (*Client, error) {
	_, err := url.Parse(baseUrl)
	if err != nil {
		return nil, err
	}

	client := &Client{
		baseUrl:    baseUrl,
		httpClient: &http.Client{},
	}

	for _, o := range opts {
		o(client)
	}

	client.Articles = &Articles{c: client}

	return client, nil
}

func (c *Client) createRequest(
	ctx context.Context,
	method string,
	url string,
	opts interface{},
	payload interface{},
) (*http.Request, error) {
	buffer := bytes.NewBuffer(nil)
	if method == http.MethodPost || method == http.MethodPut {
		j, err := json.Marshal(payload)
		if err != nil {
			return nil, err
		}
		buffer = bytes.NewBuffer(j)
	}

	q, err := query.Values(opts)
	if err != nil {
		return nil, err
	}

	url = fmt.Sprintf("%s?%s", url, q.Encode())
	url = c.baseUrl + url
	req, err := http.NewRequestWithContext(ctx, method, url, buffer)
	if err != nil {
		return nil, err
	}

	if c.apiKey != "" {
		req.Header.Add("api-key", c.apiKey)
	}

	return req, err
}

func (c *Client) get(ctx context.Context, endpoint string, opts interface{}) (*http.Response, error) {
	req, err := c.createRequest(ctx, http.MethodGet, endpoint, opts, nil)
	if err != nil {
		return nil, err
	}

	resp, err := c.httpClient.Do(req)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode != http.StatusOK {
		errorResponse, _ := unmarshalResponse[ErrorResponse](resp)
		return nil, fmt.Errorf("status: %d, error: %s", errorResponse.Status, errorResponse.Error)
	}

	return resp, nil
}

func (c *Client) save(ctx context.Context, httpMethod string, endpoint string, payload interface{}) (*http.Response, error) {
	req, err := c.createRequest(ctx, httpMethod, endpoint, nil, payload)
	if err != nil {
		return nil, err
	}

	req.Header.Add("Content-Type", "application/json")

	resp, err := c.httpClient.Do(req)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode != http.StatusOK && resp.StatusCode != http.StatusCreated {
		errorReponse, _ := unmarshalResponse[ErrorResponse](resp)
		return nil, fmt.Errorf("status: %d, error: %s", errorReponse.Status, errorReponse.Error)
	}

	return resp, nil
}

func (c *Client) post(ctx context.Context, endpoint string, payload interface{}) (*http.Response, error) {
	return c.save(ctx, http.MethodPost, endpoint, payload)
}

func (c *Client) put(ctx context.Context, endpoint string, payload interface{}) (*http.Response, error) {
	return c.save(ctx, http.MethodPut, endpoint, payload)
}

func unmarshalResponse[T interface{}](resp *http.Response) (T, error) {
	defer resp.Body.Close()
	var out T

	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return out, err
	}

	if err := json.Unmarshal(b, &out); err != nil {
		return out, err
	}

	return out, nil
}
