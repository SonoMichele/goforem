package goforem

import (
	"context"
	"fmt"
)

type ArticleState string

const (
	StateFresh  ArticleState = "fresh"
	StateRising ArticleState = "rising"
	StateAll    ArticleState = "all"
)

type Articles struct {
	c *Client
}

// GET articles
func (a *Articles) Published(ctx context.Context, opts PublishedArticlesOpts) ([]Article, error) {
	resp, err := a.c.get(ctx, "articles", opts)
	if err != nil {
		return []Article{}, err
	}

	articles, err := unmarshalResponse[[]Article](resp)
	if err != nil {
		return []Article{}, err
	}

	return articles, nil
}

// GET articles/latest
func (a *Articles) Latest(ctx context.Context, opts LatestArticlesOpts) ([]Article, error) {
	resp, err := a.c.get(ctx, "articles/latest", opts)
	if err != nil {
		return []Article{}, err
	}

	articles, err := unmarshalResponse[[]Article](resp)
	if err != nil {
		return []Article{}, err
	}

	return articles, nil
}

// GET articles/{id}
func (a *Articles) ById(ctx context.Context, id int) (ArticleVariant, error) {
	resp, err := a.c.get(ctx, fmt.Sprintf("articles/%d", id), nil)
	if err != nil {
		return ArticleVariant{}, err
	}

	article, err := unmarshalResponse[ArticleVariant](resp)
	if err != nil {
		return ArticleVariant{}, err
	}

	return article, nil
}

// GET articles/{username}/{slug}
func (a *Articles) ByPath(ctx context.Context, username string, slug string) (ArticleVariant, error) {
	resp, err := a.c.get(ctx, fmt.Sprintf("articles/%s/%s", username, slug), nil)
	if err != nil {
		return ArticleVariant{}, nil
	}

	article, err := unmarshalResponse[ArticleVariant](resp)
	if err != nil {
		return ArticleVariant{}, nil
	}

	return article, nil
}

// GET articles/me/published
// requires ApiKey to be set
func (a *Articles) UserPublished(ctx context.Context, opts UserArticlesOpts) ([]Article, error) {
	resp, err := a.c.get(ctx, "articles/me/published", opts)
	if err != nil {
		return []Article{}, err
	}

	articles, err := unmarshalResponse[[]Article](resp)
	if err != nil {
		return []Article{}, err
	}

	return articles, nil
}

// GET articles/me/unpublished
// requires ApiKey to be set
func (a *Articles) UserUnpublished(ctx context.Context, opts UserArticlesOpts) ([]Article, error) {
	resp, err := a.c.get(ctx, "articles/me/unpublished", opts)
	if err != nil {
		return []Article{}, err
	}

	articles, err := unmarshalResponse[[]Article](resp)
	if err != nil {
		return []Article{}, err
	}

	return articles, nil
}

// GET articles/me/all
// requires ApiKey to be set
func (a *Articles) UserAll(ctx context.Context, opts UserArticlesOpts) ([]Article, error) {
	resp, err := a.c.get(ctx, "articles/me/all", opts)
	if err != nil {
		return []Article{}, err
	}

	articles, err := unmarshalResponse[[]Article](resp)
	if err != nil {
		return []Article{}, err
	}

	return articles, nil
}

// POST articles
func (a *Articles) Create(ctx context.Context, payload CreateArticlePayload) (ArticleVariant, error) {
	resp, err := a.c.post(ctx, "articles", payload)
	if err != nil {
		return ArticleVariant{}, err
	}

	article, err := unmarshalResponse[ArticleVariant](resp)
	if err != nil {
		return ArticleVariant{}, err
	}

	return article, nil
}

// PUT articles/{id}
func (a *Articles) Update(ctx context.Context, id int, payload UpdateArticlePayload) (ArticleVariant, error) {
	resp, err := a.c.put(ctx, fmt.Sprintf("articles/%d", id), payload)
	if err != nil {
		return ArticleVariant{}, err
	}

	article, err := unmarshalResponse[ArticleVariant](resp)
	if err != nil {
		return ArticleVariant{}, err
	}

	return article, nil
}
