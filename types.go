package goforem

type PublishedArticlesOpts struct {
	Page         int          `url:"page,omitempty"`
	PerPage      int          `url:"per_page,omitempty"`
	Tag          string       `url:"tag,omitempty"`
	Tags         []string     `url:"tags,omitempty" del:","`
	TagsExclude  []string     `url:"tags_exclude,omitempty" del:","`
	Username     string       `url:"username,omitempty"`
	State        ArticleState `url:"state,omitempty"`
	Top          int          `url:"top,omitempty"`
	CollectionId int          `url:"collection_id,omitempty"`
}

type LatestArticlesOpts struct {
	Page    int `url:"page,omitempty"`
	PerPage int `url:"per_page,omitempty"`
}

type UserArticlesOpts LatestArticlesOpts

type Article struct {
	TypeOf                 string        `json:"type_of"`
	ID                     int           `json:"id"`
	Title                  string        `json:"title"`
	Description            string        `json:"description"`
	CoverImage             string        `json:"cover_image"`
	ReadablePublishDate    string        `json:"readable_publish_date"`
	SocialImage            string        `json:"social_image"`
	TagList                []string      `json:"tag_list"`
	Tags                   string        `json:"tags"`
	Slug                   string        `json:"slug"`
	Path                   string        `json:"path"`
	URL                    string        `json:"url"`
	CanonicalURL           string        `json:"canonical_url"`
	CommentsCount          int           `json:"comments_count"`
	PositiveReactionsCount int           `json:"positive_reactions_count"`
	PublicReactionsCount   int           `json:"public_reactions_count"`
	CollectionID           int           `json:"collection_id"`
	CreatedAt              string        `json:"created_at"`
	EditedAt               string        `json:"edited_at"`
	CrosspostedAt          string        `json:"crossposted_at"`
	PublishedAt            string        `json:"published_at"`
	LastCommentAt          string        `json:"last_comment_at"`
	PublishedTimestamp     string        `json:"published_timestamp"`
	ReadingTimeMinutes     int           `json:"reading_time_minutes"`
	BodyHTML               string        `json:"body_html"`
	BodyMarkdown           string        `json:"body_markdown"`
	User                   *User         `json:"user,omitempty"`
	Organization           *Organization `json:"organization,omitempty"`
	FlareTag               *FlareTag     `json:"flare_tag,omitempty"`
}

type ArticleVariant struct {
	Article
	Tags    []string `json:"tags"`
	TagList string   `json:"tag_list"`
}

type CreateArticlePayload struct {
	Article struct {
		Title          string   `json:"title,omitempty"`
		BodyMarkdown   string   `json:"body_markdown,omitempty"`
		Published      bool     `json:"published,omitempty"`
		Series         string   `json:"series,omitempty"`
		MainImage      string   `json:"main_image,omitempty"`
		CanonicalURL   string   `json:"canonical_url,omitempty"`
		Description    string   `json:"description,omitempty"`
		Tags           []string `json:"tags,omitempty"`
		OrganizationID int      `json:"organization_id,omitempty"`
	} `json:"article"`
}

type UpdateArticlePayload CreateArticlePayload

type User struct {
	Name            string `json:"name"`
	Username        string `json:"username"`
	TwitterUsername string `json:"twitter_username"`
	GithubUsername  string `json:"github_username"`
	WebsiteURL      string `json:"website_url"`
	ProfileImage    string `json:"profile_image"`
	ProfileImage90  string `json:"profile_image_90"`
}

type Organization struct {
	Name           string `json:"name"`
	Username       string `json:"username"`
	Slug           string `json:"slug"`
	ProfileImage   string `json:"profile_image"`
	ProfileImage90 string `json:"profile_image_90"`
}

type FlareTag struct {
	Name         string `json:"name"`
	BgColorHex   string `json:"bg_color_hex"`
	TextColorHex string `json:"text_color_hex"`
}
