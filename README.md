# goforem

A go wrapper for [forem api](developers.forem.com/api)

# Supported endpoints

- [x] articles
    - [x] GET /articles
    - [x] POST /articles
    - [x] GET /articles/latest
    - [x] GET /articles/{id}
    - [x] PUT /articles/{id}
    - [x] GET /articles/{username}/{slug}
    - [x] GET /articles/me/published (it's the same as /articles/me so I did only one)
    - [x] GET /articles/me/unpublished
    - [x] GET /articles/me/all
- [ ] comments
- [ ] follows
- [ ] followers
- [ ] listings
- [ ] organizations
- [ ] podcast_episodes
- [ ] readinglist
- [ ] users
- [ ] videso
- [ ] profile_images
- [ ] admin
